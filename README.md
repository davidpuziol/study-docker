# Study-docker

![docker_logo](./pics/docker.svg)

Esse estudo esta no repositório [study-docker](https://gitlab.com/davidpuziol/study-docker).

Foi utilizado o curso da LinuxTips para aprendizado inicial do docker, por isso mantive tudo o que foi lido na pasta [Linuxtips](./Linuxtips/), que é de total autoria deles.

Outra fonte de estudos foi o livro do [Caio Delgado](https://leanpub.com/dockerdca) que pode ser adquirido de graça.

Porém, criei um novo diretório com todas as minhas anotações voltadas para o DCA.

Costumo escrever um repositório para mim mesmo, pois é minha forma de fixar o conhecimento, então relaxem com erros de português e acentuação.

**Toda vez que estamos aprendendo uma nova ferramenta vale a pena pegar todo o roadmap para estudo da certificação e praticá-lo mesmo que no fim, não seja viável fazer a prova.**

Todo o necessário para o [DCA](https://training.mirantis.com/certification/dca-certification-exam/) será analisado durante o estudo.
Confira o guia sobre o que é pedido na prova <https://a.storyblok.com/f/146871/x/6daa07336d/docker-study-guide_v1-5-october.pdf>, caso o link tenha mudado veja sempre sobre o guia mais atualizado.

Docker precisa de um kernel linux para rodar.
AAAAA mais um tenho no windowws. Sim vc pode ter, mas por baixo ele instalou alguma coisa que virtualiza um linux pra vc, nesse caso se for o windows foi o WSL.

>O MUNDO DOS CONTAINERS É LINUX

## Montando o ambiente

A pasta [Environment](./DCA/Environment/) mostra como subir toda a infra estrutura para o estudo utilizando o [Vagrant](https://www.vagrantup.com/). Comece montando um ambiente para que possamos estudar.

## Roadmap

[Concepts](./DCA/2%20DOCKER-CONCEPTS.md) - Tem uma breve explicação minha sobre o docker focando sempre no estudo do DCA. Se quiser uma leitura ainda mais aprofundada sempre veja a [documentação oficial](https://docs.docker.com/), bem como o conteúdo da pasta da linuxtips.

[Principais Comandos](./DCA/3%20DOCKER-COMAMNDS.md) - Faz um overview geral dos comandos em docker e como utiliza-los.

[Images](./DCA/4%20DOCKER-IMAGES.md) - Mostra como funciona o build de imagens utilizando Dockerfile e algumas dicas para melhorar a criação da mesmas.

[Volumes](./DCA/5%20DOCKER-VOLUMES.md)- Uma breve explicação sobre como o docker trabalha com storages para guardar os dados gerados nos containers.

[Plugins](./DCA/6%20DOCKER-PLUGIM.md) -  Entendendo plugins para expandir as funcionalidades do docker.

[Network](./DCA/7%20DOCKER-NETWORK.md) - Quais os tipos de redes podemos utilizar no docker.

[Compose](./DCA/8%20DOCKER-COMPOSE.md) - Como criar subir vários containers docker com um arquivo docker-compose.

[Swarm](./DCA/9%20SWARM.md) - Como criar um cluster com docker swarm.

[Registry](./DCA/10%20REGISTRY.md) - Como montar um servidor registry simples para guardar as imagens.

[Swarm Parte 2](./DCA/11%20SWARM%20PARTE%202.md) - Entendendo os conceitos do swarm e escalonando containers.

[Swarm Parte 3](./DCA/11%20SWARM%20PARTE%202.md) - Entendendo os conceitos do swarm e escalonando containers.