# Plugins

<https://docs.docker.com/engine/extend/legacy_plugins/>

Plugins são utilizado para extender as funcionalidades do Docker. Os plugins servem para extender as limitações de recursos do Docker. Plugins servem para extender redes, volumes e autorização.

Vamos pensar por exemplo em um sistema de arquivos remoto para ser usado em volumes, como aws s3, precisamos extender a funcionalidade do docker.

Os plugins são distribuidos como imagens docker. Logo vc pode encontra plugins do docker no próprio dockerhub.

Plugins não são tão utilizado e tendem a ser descontinuados, porisso é considerado legacy_plugins na url.

Para instalar um plugin o comando é **docker plugin**

Para esse comando temos os seguintes subcomandos

- create
- disable  
- enable
- inspect  
- install  
- ls
- push
- rm
- set
- upgrade  

Vamos instalar um plugin famoso chamado sshfs que acessa um filesysstem de outra máquina através de ssh.Observe que referenciamos ele como uma imagem, logo poderíamos até passar a versão se quisermos. Vale lembrar que é somente um estudo e não deve ser utilizado em produção, é só para entender o uso de plugins.

```bash
agrant@worker1:~$ docker plugin install vieux/sshfs
Plugin "vieux/sshfs" is requesting the following privileges:
 - network: [host]
 - mount: [/var/lib/docker/plugins/]
 - mount: []
 - device: [/dev/fuse]
 - capabilities: [CAP_SYS_ADMIN]
Do you grant the above permissions? [y/N] y
latest: Pulling from vieux/sshfs
Digest: sha256:1d3c3e42c12138da5ef7873b97f7f32cf99fb6edde75fa4f0bcf9ed277855811
52d435ada6a4: Complete 
Installed plugin vieux/sshfs
vagrant@worker1:~$
vagrant@worker1:~$ docker plugin ls
ID             NAME                 DESCRIPTION               ENABLED
6915ebc10c2d   vieux/sshfs:latest   sshFS plugin for Docker   true 
```

<https://github.com/vieux/docker-volume-sshfs>
Como vamos criar um volume usando ssh com esse driver na máquina2 vamos precisar habilitar no worker2 o PasswordAuthetication no arquivo /etc/ssh/ssh_config.

depois reiniciar o daemon do serviço com sudo systemctl restart sshd

```bash
# descomentei a linha para ter o yes no PasswordAuthentication
[vagrant@worker2 ~]$ sudo vi /etc/ssh/ssh_config
[vagrant@worker2 ~]$ sudo systemctl restart sshd
[vagrant@worker2 ~]$ cat /etc/ssh/ssh_config
#       $OpenBSD: ssh_config,v 1.34 2019/02/04 02:39:42 dtucker Exp $

#......

Host *
#   ForwardAgent no
#   ForwardX11 no
  PasswordAuthentication yes
#   HostbasedAuthentication no
#   GSSAPIAuthentication no
#   GSSAPIDelegateCredentials no
# ....
#  /etc/ssh/ssh_config.d/  which will be automatically included below
Include /etc/ssh/ssh_config.d/*.conf
[vagrant@worker2 ~]$ ip -c -br a
lo               UNKNOWN        127.0.0.1/8 ::1/128 
eth0             UP             10.0.2.15/24 fe80::5054:ff:fe27:8b50/64 
# para pegar o ip dessa maquina
eth1             UP             192.168.56.120/24 fe80::a00:27ff:fe50:4d11/64 
```

Agora vamos criar um volume usando esse driver, mas da worker1. como estamos dentro do vagrant temos um usuário vagrant em todas as máquinas e o password é vagrant também. Vamos utilizar esse usuário.

```bash
# -d foi para escolher o driver 
# -o é options
vagrant@worker1:~$ docker volume create -d vieux/sshfs --name remotevolume -o sshcmd=vagrant@192.168.56.120:/vagrant -o password=vagrant
remotevolume
# conferindo se o volume esta disponível
vagrant@worker1:~$ docker volume ls
DRIVER               VOLUME NAME
vieux/sshfs:latest   remotevolume
vagrant@worker1:~$ docker volume inspect remotevolume | jq
[
  {
    "CreatedAt": "0001-01-01T00:00:00Z",
    "Driver": "vieux/sshfs:latest",
    "Labels": {},
    "Mountpoint": "/mnt/volumes/39cc9d0f99226f9dcafb42710622b6d6",
    "Name": "remotevolume",
    "Options": {
        #veja a insegurança, mostra o password
      "password": "vagrant",
      "sshcmd": "vagrant@192.168.56.120:/vagrant"
    },
    "Scope": "local"
  }
]
vagrant@worker1:~$ 
```

conferindo o que temos na pasta.

```bash
vagrant@worker1:~$ docker container run -it --rm -v remotevolume:/data alpine ls /data
```

# NFS Plugin

<https://registry.hub.docker.com/r/trajano/glusterfs-volume-plugin>

Vamos testar o NFS agora na máquina master que servirá como um servidor de arquivos na máquina master é necessário instalar o pacote nfs-server e no worker o nfs-common

master

```bash
vagrant@master:~$ sudo apt-get install nfs-server -y
vagrant@master:~$ mkdir -p /home/vagrant/storage
vagrant@master:~$ sudo echo "/home/vagrant/storage 192.168.56.0/24(rw)" > /etc/exports
vagrant@master:~$ cat /etc/exports 
vagrant@master:~$ mkdir -p /home/vagrant/storage
vagrant@master:~$ sudo vi /etc/exports 
vagrant@master:~$ cat /etc/exports 
/home/vagrant/storage 192.168.56.0/24(rw)
# criando um arquivo e reiniciando o sistema e conferindo o que esta exportado
vagrant@master:~$ echo "soh testando" > /home/vagrant/storage/arquivo
vagrant@master:~$ sudo systemctl restart nfs-server.service 
vagrant@master:~$ showmount -e
Export list for master.docker-dca.example:
/home/vagrant/storage 192.168.56.0/24
vagrant@master:~$ 
```

worker

```bash
sudo apt-get install nfs-common -y
docker plugin install trajano/glusterfs-volume-plugin
docker volume create -d trajano/glusterfs-volume-plugin --opt servers=192.168.56.100 volume_nfs
vagrant@worker1:~$ docker volume inspect volume_nfs | jq
[
  {
    "CreatedAt": "0001-01-01T00:00:00Z",
    "Driver": "trajano/nfs-volume-plugin:latest",
    "Labels": {},
    "Mountpoint": "",
    "Name": "volume_nfs",
    "Options": {
      "device": "192.168.56.100:/home/vagrant/storage",
      "nfsopts": "hard,proto=tcp,nfsvers=3,intr,nolock"
    },
    # escopo local é para sómente a maquina, mas com o global qualquer maquina do cluster pode usar.
    "Scope": "global",
    "Status": {
      "args": [
        "-t",
        "nfs",
        "-o",
        "hard,proto=tcp,nfsvers=3,intr,nolock",
        "192.168.56.100:/home/vagrant/storage"
      ],
      "mounted": false
    }
  }
]
vagrant@worker1:~$ 
```

docker run --rm -v nfs_volume:/data alpine ls /data


