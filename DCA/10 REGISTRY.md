# Registry

Existem vários registries para guardar as nossas imagens.

- Dockerhub
- Amazon Elastic Container Registry (ECR)
- Google Containe Registry (GCR)
- Azure Container Registry (ACR)
- Gitlab Container Registry
- Google Artifact Registry (GAR)
- Github Package Registry
- Red Hat Quay

... E muitos outros, mas no fim todos eles utilizam a mesma imagem base chamada registry v2 o que muda é só a interface gráfica e alguns recursos que trabalham em cima da imagem pronta, como um scan vulnerabilidade por exemplo.

Porém é interessante no nosso cluster termos um private registry por alguns motivos.

![privateregistry](./pics/privateregistry.png)

Motivos para se ter um private registry:

- Dockerhub possui limitações de 100 imagens (pull) que pode baixar a cada 6 horas no modo anônimo. Se você estiver logado pode chegar a 200 e para utilizar acima disso vc precisa pagar <https://docs.docker.com/docker-hub/download-rate-limit/#:~:text=Docker%20Hub%20limits%20the%20number,pulls%20per%206%20hour%20period.>. Se vc tem 10 hosts com containers distribuidos entre eles que usam uma mesma base, é preciso fazer um pull de todas as camadas para cada um hosts (cada camada é uma imagem).  O limite de 100 ou 200 se foi em poucos minutos. Se vc tiver um private registry ele baixa uma vez as camadas e fornece para os hosts. Funcionaria como um cache, mas além disso consegue fazer integração com vários outros registries.
- Velocidade para download das imagens uma vez que as imagens já estão na sua infra, diminuindo o uso de banda de network.
- Segurança para executar um push e pull das imagens dentro da prórpria infra, evitando ataque como man in the middle para registries externos.

>Se vc tiver em um cloud por exemplo vale a pena ter um private registry?
\
>Esse serviço costuma ser muito barato Uma vez que vc não precisa gerênciar certificado ssl e esteja utilizando o registry da sua própria cloud, velocidade e segurança estão garantidas.

Vamos adicionar o registry ao nosso cluster swarm

```bash
vagrant@registry:~$ docker swarm join --token SWMTKN-1-5uadpu9vr0rcq1hbjtzv7exb34vyreuc9l4gkgggg0y1x0c471-75b4qqhg4ez89w5h49x0dwdz3 10.10.10.100:2377
This node joined a swarm as a worker.
vagrant@registry:~$ 

vagrant@master:~$ docker node ls
ID                            HOSTNAME                      STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
jdmwyhbti8s3fnmd17lw79rhw *   master.docker-dca.example     Ready     Active         Leader           20.10.17
qvp6um8mstrgrlhhfpjj6khdc     registry.docker-dca.example   Ready     Active                          20.10.17
rxgmhpjtky4s6mktwis2jyr99     worker1.docker-dca.example    Ready     Active                          20.10.17
7980uc978wk928ncb6esv3jy3     worker2.docker-dca.example    Ready     Active         
```

>Geralmente precisamos utilizar um certificado ssl em nossas comunicações com o registry, mas como estamos fazendo local não utilizaremos.

>Para isso somente vamos configurar o nosso daemon.json `EM TODOS OS NODES` que é o arquivo de configuração do docker para o seguinte.

Sem um ssl não é necessário a autenticação como um docker login para conferir os dados, simplesmente vc pode fazer um push da imagem sem conferência de dados.

Vamos precisar configurar o registry em todas as máquinas e logo em seguida precisamos restartar docker.

![registryconfig](./pics/insecureregistry.png)

![restart](./pics/./restartdocker.png)

Agora na host registry vamos deployar o nosso container registry. Veja a imagem e a porta que é a unica coisa de novo por aqui.

- imagem = registry:2
- porta = 5000

![containerregistry](./pics/deployregistry.png)

Agora vamos fazer um pull de uma imagem do dockerhub.

```bash
agrant@registry:~$ docker pull alpine
Using default tag: latest
latest: Pulling from library/alpine
Digest: sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c
Status: Image is up to date for alpine:latest
docker.io/library/alpine:latest
vagrant@registry:~$ docker image ls
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
registry     2         773dbf02e42e   5 weeks ago   24.1MB
alpine       latest    e66264b98777   5 weeks ago   5.53MB
vagrant@registry:~$ 
```

>Essa imagem esta no registry?

<details>
  <summary>Reposta</summary>
  
Não, ele somente esta no host. As imagens para serem direcionadas ao nosso registry precisam taggeadas para ele. Se vc fizer push sem taggear ele vai entender que é para o registry padrão, dockehub.
</details>

Vamos fazer uma tag e subir a imagem para o nosso registry

```bash
vagrant@registry:~$ docker image ls
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
registry     2         773dbf02e42e   5 weeks ago   24.1MB
alpine       latest    e66264b98777   5 weeks ago   5.53MB
vagrant@registry:~$ docker image tag alpine:latest registry.docker-dca.example:5000/alpine
vagrant@registry:~$ docker image ls
REPOSITORY                                TAG       IMAGE ID       CREATED       SIZE
registry                                  2         773dbf02e42e   5 weeks ago   24.1MB
alpine                                    latest    e66264b98777   5 weeks ago   5.53MB
registry.docker-dca.example:5000/alpine   latest    e66264b98777   5 weeks ago   5.53MB
# fazendo o push
vagrant@registry:~$ docker push registry.docker-dca.example:5000/alpine
Using default tag: latest
The push refers to repository [registry.docker-dca.example:5000/alpine]
24302eb7d908: Pushed 
latest: digest: sha256:4ff3ca91275773af45cb4b0834e12b7eb47d1c18f770a0b151381cd227f4c253 size: 528
vagrant@registry:~$ 
```

Esse registry que levantamos não possui tipo de interface visual, mas tem uma api de consulta que podemos utilizar fazendo um curl ou indo no próprio navegador.

<http://registry.docker-dca.example:5000/v2/_catalog>

```bash
vagrant@master:~$ curl http://registry.docker-dca.example:5000/v2/_catalog
{"repositories":["alpine"]}
vagrant@master:~$ curl -s http://registry.docker-dca.example:5000/v2/_catalog | jq
{
  "repositories": [
    "alpine"
  ]
}
```

As imagens são guardadas no volume do container.

```bash
vagrant@registry:~$ docker container inspect registry | grep Source
                "Source": "/var/lib/docker/volumes/7c60f11e7e59839e0e57b6aab78de979065c293624745830b9b57686c95d4286/_data",
vagrant@registry:~$ docker volume 
create   inspect  ls       prune    rm       
vagrant@registry:~$ docker volume inspect 7c60f11e7e59839e0e57b6aab78de979065c293624745830b9b57686c95d4286 
[
    {
        "CreatedAt": "2022-07-04T03:21:46Z",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/7c60f11e7e59839e0e57b6aab78de979065c293624745830b9b57686c95d4286/_data",
        "Name": "7c60f11e7e59839e0e57b6aab78de979065c293624745830b9b57686c95d4286",
        "Options": null,
        "Scope": "local"
    }
]
# vamos conferir
vagrant@registry:~$ sudo tree /var/lib/docker/volumes/7c60f11e7e59839e0e57b6aab78de979065c293624745830b9b57686c95d4286
/var/lib/docker/volumes/7c60f11e7e59839e0e57b6aab78de979065c293624745830b9b57686c95d4286
└── _data
    └── docker
        └── registry
            └── v2
                ├── blobs
                │   └── sha256
                │       ├── 24
                │       │   └── 2408cc74d12b6cd092bb8b516ba7d5e290f485d3eb9672efc00f0583730179e8
                │       │       └── data
                │       ├── 4f
                │       │   └── 4ff3ca91275773af45cb4b0834e12b7eb47d1c18f770a0b151381cd227f4c253
                │       │       └── data
                │       └── e6
                │           └── e66264b98777e12192600bf9b4d663655c98a090072e1bab49e233d7531d1294
                │               └── data
                └── repositories
                    └── alpine #Aqui...
                        ├── _layers
                        │   └── sha256
                        │       ├── 2408cc74d12b6cd092bb8b516ba7d5e290f485d3eb9672efc00f0583730179e8
                        │       │   └── link
                        │       └── e66264b98777e12192600bf9b4d663655c98a090072e1bab49e233d7531d1294
                        │           └── link
                        ├── _manifests
                        │   ├── revisions
                        │   │   └── sha256
                        │   │       └── 4ff3ca91275773af45cb4b0834e12b7eb47d1c18f770a0b151381cd227f4c253
                        │   │           └── link
                        │   └── tags
                        │       └── latest
                        │           ├── current
                        │           │   └── link
                        │           └── index
                        │               └── sha256
                        │                   └── 4ff3ca91275773af45cb4b0834e12b7eb47d1c18f770a0b151381cd227f4c253
                        │                       └── link
                        └── _uploads
```

vamos baixar varias imagens pro nosso lab

- nginx
- mysq:5.7
- wordpress
- traefik:v2.4

Precisariamos fazer um docker pull, docker image tag e docker push de cada uma dessas imagens.

Vamos automatizar isso rápido

```bash
for image in 'nginx' 'mysql:5.7' 'wordpress' 'traefik'
do
docker pull $image
docker image tag $image registry.docker-dca.example:5000/$image
docker push registry.docker-dca.example:5000/$image
done
#... Conferindo
vagrant@master:~$ curl -s http://registry.docker-dca.example:5000/v2/_catalog | jq
{
  "repositories": [
    "alpine",
    "mysql",
    "nginx",
    "traefik",
    "wordpress"
  ]
}
# Conferindo as tags..
vagrant@master:~$ curl -s http://registry.docker-dca.example:5000/v2/traefik/tags/list | jq
{
  "name": "traefik",
  "tags": [
    "latest"
  ]
}
vagrant@master:~$ curl -s http://registry.docker-dca.example:5000/v2/wordpress/tags/list | jq
{
  "name": "wordpress",
  "tags": [
    "latest"
  ]
}
vagrant@master:~$ curl -s http://registry.docker-dca.example:5000/v2/mysql/tags/list | jq
{
  "name": "mysql",
  "tags": [
    "5.7"
  ]
}
#...
```
