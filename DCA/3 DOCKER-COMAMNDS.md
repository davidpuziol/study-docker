# Comandos

A Docker passou por uma reformulação de sintaxe. Alguns comandos velhos ainda funcionam mas vale lembrar que para uma prova de DCA devemos começar a utilizar sempre os comandos novos.

**docker ps** (sintaxe antiga)\
**docker container ls** (sintaxe nova)

**docker info** (sintaxe antiga)\
**docker system info** (sintaxe nova)

Tudo que cair na prova referente a sintaxe antiga será considerado erro. Esses comando antigos não são considerado aliases, ainda funcionam, mas serão descontinuados. Então acostume-se com o novo.

Algumas coisas são de fato alias.\
**docker container ls**\
**docker container list**\
Qualquer um desses dois estão na sintaxe nova, logo para a prova qualquer um esta correto. Observe que a estrutura se mantém quando é um alias.

## Auto Complete

Para ajudar o auto complete é bom ser instalado.
<https://docs.docker.com/compose/completion/>

para isso é necessário ter o pacote bash-completion instalado, instale utilizando o gerenciador de pacotes da sua distro e logo após digite os comandos abaixo.

Para o comando docker

```bash
sudo curl -L curl https://raw.githubusercontent.com/docker/docker-ce/master/components/cli/contrib/completion/bash/docker -o /etc/bash_completion.d/docker.sh
```

Para o comando docker-compose que iremos ver mais a frente

```bash
sudo curl -L https://raw.githubusercontent.com/docker/compose/master/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
```

## --Help

Para ajuda do docker ele oferece um help bem explicativo.

```bash
❯ docker --help

Usage:  docker [OPTIONS] COMMAND

A self-sufficient runtime for containers

Options:
      --config string      Location of client config files (default "/home/david/.docker")
  -c, --context string     Name of the context to use to connect to the daemon (overrides DOCKER_HOST env var and default context set with "docker context use")
  -D, --debug              Enable debug mode
  -H, --host list          Daemon socket(s) to connect to
  -l, --log-level string   Set the logging level ("debug"|"info"|"warn"|"error"|"fatal") (default "info")
      --tls                Use TLS; implied by --tlsverify
      --tlscacert string   Trust certs signed only by this CA (default "/home/david/.docker/ca.pem")
      --tlscert string     Path to TLS certificate file (default "/home/david/.docker/cert.pem")
      --tlskey string      Path to TLS key file (default "/home/david/.docker/key.pem")
      --tlsverify          Use TLS and verify the remote
  -v, --version            Print version information and quit

Management Commands:
  builder     Manage builds
  buildx*     Docker Buildx (Docker Inc., v0.8.2-docker)
  compose*    Docker Compose (Docker Inc., 2.6.0)
  config      Manage Docker configs
  container   Manage containers
  context     Manage contexts
  image       Manage images
  manifest    Manage Docker image manifests and manifest lists
  network     Manage networks
  node        Manage Swarm nodes
  plugin      Manage plugins
  secret      Manage Docker secrets
  service     Manage services
  stack       Manage Docker stacks
  swarm       Manage Swarm
  system      Manage Docker
  trust       Manage trust on Docker images
  volume      Manage volumes

Commands:
  attach      Attach local standard input, output, and error streams to a running container
  build       Build an image from a Dockerfile
  commit      Create a new image from a container's changes
  cp          Copy files/folders between a container and the local filesystem
  create      Create a new container
  diff        Inspect changes to files or directories on a container's filesystem
  events      Get real time events from the server
  exec        Run a command in a running container
  export      Export a container's filesystem as a tar archive
  history     Show the history of an image
  images      List images
  import      Import the contents from a tarball to create a filesystem image
  info        Display system-wide information
  inspect     Return low-level information on Docker objects
  kill        Kill one or more running containers
  load        Load an image from a tar archive or STDIN
  login       Log in to a Docker registry
  logout      Log out from a Docker registry
  logs        Fetch the logs of a container
  pause       Pause all processes within one or more containers
  port        List port mappings or a specific mapping for the container
  ps          List containers
  pull        Pull an image or a repository from a registry
  push        Push an image or a repository to a registry
  rename      Rename a container
  restart     Restart one or more containers
  rm          Remove one or more containers
  rmi         Remove one or more images
  run         Run a command in a new container
  save        Save one or more images to a tar archive (streamed to STDOUT by default)
  search      Search the Docker Hub for images
  start       Start one or more stopped containers
  stats       Display a live stream of container(s) resource usage statistics
  stop        Stop one or more running containers
  tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
  top         Display the running processes of a container
  unpause     Unpause all processes within one or more containers
  update      Update configuration of one or more containers
  version     Show the Docker version information
  wait        Block until one or more containers stop, then print their exit codes

Run 'docker COMMAND --help' for more information on a command.

To get more help with docker, check out our guides at https://docs.docker.com/go/guides/
```

## Principais comandos

**docker system**: traz resultados do sistema
**docker container** trabalha com os containers
**docker image** manipula as imagens
**docker volume** trabalha com os volumes
**docker network** trabalha com a network

os comandos principais possuem comandos secundários, muitos são iguais inclusive

Vamos executar este comando para rodar um container hello world do docker.

```bash
vagrant@master:~$ docker container run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

O que ele fez? Baixou uma imagem, rodou a imagem e morreu. Agora, Agora ja temos uma imagem, um container morto, vamos aos testes.

### ls ou list

lista as o que esta sendo de fato utilizado no momento

```bash
vagrant@master:~$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
vagrant@master:~$ docker images ls
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE
vagrant@master:~$ docker volume ls
DRIVER    VOLUME NAME
vagrant@master:~$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
487679e6deca   bridge    bridge    local
d1a3aee195f4   host      host      local
fd926f9c1bbe   none      null      local
vagrant@master:~$ 
```

Veja que ele não listou o container nem a imagem hello-world, pois ele já morreu.

Se quisermos ver tudo utilizamos o **--all**

```bash
vagrant@master:~$ docker images --all
REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
hello-world   latest    feb5d9fea6a5   8 months ago   13.3kB
```

```bash
vagrant@master:~$ docker container list --all
CONTAINER ID   IMAGE         COMMAND    CREATED         STATUS                     PORTS     NAMES
6fc6ee0898d7   hello-world   "/hello"   3 minutes ago   Exited (0) 3 minutes ago             charming_aryabhata
```

```bash
para ver somente os ids dos recursos

vagrant@master:~$ docker container list --all -q
6fc6ee0898d7

vagrant@master:~$ docker images -q
feb5d9fea6a5

vagrant@master:~$ docker network ls -q
487679e6deca
d1a3aee195f4
fd926f9c1bbe

vagrant@master:~$ docker volume ls -q
```

## Seach

<https://docs.docker.com/engine/reference/commandline/search/>

serve para pesquiser plugins e imagens no registry. Evite usar imagem não oficial para sua segurança.

No comando abaixo apliquei um filter no search para listar somente as oficiais

```bash
vagrant@master:~$ docker search --filter is-official=true debian
NAME          DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
ubuntu        Ubuntu is a Debian-based Linux operating sys…   14456     [OK]       
debian        Debian is a Linux distribution that's compos…   4341      [OK]       
neurodebian   NeuroDebian provides neuroscience research s…   91        [OK]       
vagrant@master:~$ 
```

Agora adicionei mais um filtro para pegar somente com estrelas acima de 5000 e veio somente a oficial mesmo.

```bash
vagrant@master:~$ docker search --filter is-official=true --filter stars=5000 debian
NAME      DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
ubuntu    Ubuntu is a Debian-based Linux operating sys…   14456     [OK]       
```

## Pull

Serve para baixar a imagem que queremos do registry. Aproveitando o filtro acima sabemos que vamos baixar o ubuntu.

Essas imagens ficam no [dockerhub](https://hub.docker.com/) que é o registry oficial do docker.

```bash
vagrant@master:~$ docker image pull ubuntu
Using default tag: latest
latest: Pulling from library/ubuntu
405f018f9d1d: Pull complete 
Digest: sha256:b6b83d3c331794420340093eb706a6f152d9c1fa51b262d9bf34594887c2c7ac
Status: Downloaded newer image for ubuntu:latest
docker.io/library/ubuntu:latest
vagrant@master:~$ docker image ls
REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
ubuntu        latest    27941809078c   13 days ago    77.8MB
hello-world   latest    feb5d9fea6a5   8 months ago   13.3kB
```

## run, attach, start

Vamos rodar a imagem do debian (ultimo parametro), que não temos no nosso pull. O docker procura a imagem com o nome passado localmente se não encontar ele tenta fazer o pull automaticamente dos registryes que temos, nesse caso somente o dockerhub por padrão.

-d (detach)-i (interative) -t (tty) = -dit \
-name passamos um nome para identificar o container, se não tivesse passado ele daria um nome aleatório, veja na tabela em NAMES\
-hostname passamos um hostname para ele já colocar em /etc/hostname dentro do container.

```bash
vagrant@master:~$ docker container run -dit --name debiandca --hostname mydebian.local debian
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
e756f3fdd6a3: Pull complete 
Digest: sha256:3f1d6c17773a45c97bd8f158d665c9709d7b29ed7917ac934086ad96f92e4510
Status: Downloaded newer image for debian:latest
30755da9310e0b921213f457b7bb6dac877443e00b775dbe4e55d7b91b0edb72
vagrant@master:~$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED         STATUS         PORTS     NAMES
30755da9310e   debian    "bash"    7 seconds ago   Up 5 seconds             debiandca
```

O que mantém esse container vivo é o bash que é o comando que mantém ele vivo. Se executarmos algo nesse bash que de um exit code diferente de zero, o container morrerá.

Vamos conferir o /etc/hostname e vejamos o que acontece se listarmos o container novamente, vamos attachar no continer para isso.

```bash
vagrant@master:~$ docker container attach debiandca 
root@mydebian:/# cat /etc/host 
host.conf  hostname   hosts      
root@mydebian:/# cat /etc/hostname 
mydebian.local
root@mydebian:/# exit

vagrant@master:~$ docker container ls -a
CONTAINER ID   IMAGE         COMMAND    CREATED          STATUS                      PORTS     NAMES
30755da9310e   debian        "bash"     24 minutes ago   Exited (0) 5 seconds ago              debiandca
6fc6ee0898d7   hello-world   "/hello"   51 minutes ago   Exited (0) 51 minutes ago             charming_aryabhata
```

Observe que o container sumiu, pois o exit deu uma saída diferente de zero.

Para dar um start no container novamente...

```bash
vagrant@master:~$ docker container start debiandca 
debiandca
```

Para sair do container sem matá-lo existe uma sequencia de teclas chamada de **Read Escape Sequence** que é o **CTRL + P + Q**. Vamos attachar nele e sair com o Read Escape Sequence e conferir.

```bash
vagrant@master:~$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED          STATUS         PORTS     NAMES
30755da9310e   debian    "bash"    31 minutes ago   Up 5 minutes             debiandca
vagrant@master:~$ docker attach debiandca 
root@mydebian:/# read escape sequence
vagrant@master:~$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED          STATUS         PORTS     NAMES
30755da9310e   debian    "bash"    31 minutes ago   Up 6 minutes             debiandca
```

Esta vivo ainda.

## logs

vai trazer os logs da saída padrão o container. No caso eu executei alguns comandinhos para teste.

```bash
vagrant@master:~$ docker container logs debiandca 
root@mydebian:/# exit
exit
root@mydebian:/# cat /etc/hosts 
127.0.0.1 localhost
::1 localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.17.0.2 mydebian.local mydebian
vagrant@master:~$ 
```

## stop e rm

Para parar um container utilizamos o stop. Vamos verificar o que temos rodando, para o container, verificar novamente para ver que não tem nada, mas com o comando list --all podemos ver que ele ainda existe, mas não esta rodando. Depois vamos remover ele de fato e conferir novamente.

```bash
vagrant@master:~$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED          STATUS          PORTS     NAMES
30755da9310e   debian    "bash"    45 minutes ago   Up 20 minutes             debiandca
vagrant@master:~$ docker container stop debiandca
debiandca
vagrant@master:~$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
vagrant@master:~$ docker container list --all
CONTAINER ID   IMAGE         COMMAND    CREATED             STATUS                         PORTS     NAMES
30755da9310e   debian        "bash"     46 minutes ago      Exited (137) 18 seconds ago              debiandca
6fc6ee0898d7   hello-world   "/hello"   About an hour ago   Exited (0) About an hour ago             charming_aryabhata
vagrant@master:~$ docker container rm debiandca 
debiandca
vagrant@master:~$ docker container list --all
CONTAINER ID   IMAGE         COMMAND    CREATED             STATUS                         PORTS     NAMES
6fc6ee0898d7   hello-world   "/hello"   About an hour ago   Exited (0) About an hour ago             charming_aryabhata
vagrant@master:~$ 

Remover um container remove a imagem? Não.

Vamos conferir e ver que a imagem ainda esta lá. Se executarmos outro container com a mesma imagem, ele não precisará fazer um pull da imagem.

```bash
vagrant@master:~$ docker image ls --all
REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
ubuntu        latest    27941809078c   13 days ago    77.8MB
debian        latest    4eacea30377a   3 weeks ago    124MB
hello-world   latest    feb5d9fea6a5   8 months ago   13.3kB
vagrant@master:~$ 
```

Se quisermos remover um container que esta em execução o que acontecerá? Teremos um erro. Para isso usando o -f para forçar.

```bash
vagrant@master:~$ docker container run -dit --name debiandca --hostname mydebian.local debian
63c49ea76047c2f7d852926a6f6f6dfea051b8717f910989736dd3e1d3068614
vagrant@master:~$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED          STATUS          PORTS     NAMES
63c49ea76047   debian    "bash"    19 seconds ago   Up 19 seconds             debiandca
vagrant@master:~$ docker container rm debiandca
Error response from daemon: You cannot remove a running container 63c49ea76047c2f7d852926a6f6f6dfea051b8717f910989736dd3e1d3068614. Stop the container before attempting removal or force remove
vagrant@master:~$ docker container rm -f debiandca 
debiandca
vagrant@master:~$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

O comando **rm* serve para remover, seja uma imagem, uma network, um container, um volume...

o rm remove uma lista, logo podemos passar uma completa para ele remover tudo se quisermos.

docker image rm -f $(docker image ls -q)

para remover todos os containers\
docker container rm -f $(docker container ls -aq)

```bash
vagrant@master:~$ docker image ls
REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
ubuntu        latest    27941809078c   13 days ago    77.8MB
debian        latest    4eacea30377a   3 weeks ago    124MB
hello-world   latest    feb5d9fea6a5   8 months ago   13.3kB
vagrant@master:~$ docker image rm -f $(docker image ls -q)
Untagged: ubuntu:latest
Untagged: ubuntu@sha256:b6b83d3c331794420340093eb706a6f152d9c1fa51b262d9bf34594887c2c7ac
Deleted: sha256:27941809078cc9b2802deb2b0bb6feed6c236cde01e487f200e24653533701ee
Deleted: sha256:a790f937a6aea2600982de54a5fb995c681dd74f26968d6b74286e06839e4fb3
Untagged: debian:latest
Untagged: debian@sha256:3f1d6c17773a45c97bd8f158d665c9709d7b29ed7917ac934086ad96f92e4510
Deleted: sha256:4eacea30377a698ef8fbec99b6caf01cb150151cbedc8e0b1c3d22f134206f1a
Deleted: sha256:e7597c345c2eb11bce09b055d7c167c526077d7c65f69a7f3c6150ffe3f557ea
Untagged: hello-world:latest
Untagged: hello-world@sha256:13e367d31ae85359f42d637adf6da428f76d75dc9afeb3c21faea0d976f5c651
Deleted: sha256:feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412
Deleted: sha256:e07ee1baac5fae6a26f30cabfe54a36d3402f96afda318fe0a96cec4ca393359
vagrant@master:~$ docker image ls
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE
vagrant@master:~$
```

## cp e exec

cp é o comando de copy. Funciona como se estivessemos copiando via ssh algo para dentro do container.

Vamos criar um arquivo e tranferi-lo para dentro do container, logo vamos executar um comando sem precisar attachar no container e verificar o arquivo.

```bash
vagrant@master:~$ docker container run -dit --name debiandca --hostname mydebian.local debian
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
e756f3fdd6a3: Pull complete 
Digest: sha256:3f1d6c17773a45c97bd8f158d665c9709d7b29ed7917ac934086ad96f92e4510
Status: Downloaded newer image for debian:latest
a0a45037b974b67619c7b758c6fabe7c87a7111720b47bcf477a269293dda27b
vagrant@master:~$ echo "tribulus terrestris tetris" > /tmp/arquivo
vagrant@master:~$ cat /tmp/arquivo 
tribulus terrestris tetris
vagrant@master:~$ docker container cp /tmp/arquivo debiandca:/tmp/arquivo
vagrant@master:~$ docker container exec debiandca cat /tmp/arquivo
tribulus terrestris tetris
vagrant@master:~$ 
```
